#include <errno.h>
#include <string.h>

#include "helpers.h"
#include "gpio.h"
#include "i2c.h"

#include "tcal6416.h"

#define TCAL6416_CMD_INPUT_PORT_0 0x00
#define TCAL6416_CMD_INPUT_PORT_1 0x01
#define TCAL6416_CMD_OUTPUT_PORT_0 0x02
#define TCAL6416_CMD_OUTPUT_PORT_1 0x03
#define TCAL6416_CMD_POLARITY_INVERSION_PORT_0 0x04
#define TCAL6416_CMD_POLARITY_INVERSION_PORT_1 0x05
#define TCAL6416_CMD_CONFIGURATION_PORT_0 0x06
#define TCAL6416_CMD_CONFIGURATION_PORT_1 0x07
#define TCAL6416_CMD_OUTPUT_DRIVE_STRNGTH_PORT_0_0 0x40
#define TCAL6416_CMD_OUTPUT_DRIVE_STRNGTH_PORT_0_1 0x41
#define TCAL6416_CMD_OUTPUT_DRIVE_STRNGTH_PORT_1_0 0x42
#define TCAL6416_CMD_OUTPUT_DRIVE_STRNGTH_PORT_1_1 0x43
#define TCAL6416_CMD_LATCH_PORT_0 0x44
#define TCAL6416_CMD_LATCH_PORT_1 0x45
#define TCAL6416_CMD_PULL_UP_DOWN_ENABLE_PORT_0 0x46
#define TCAL6416_CMD_PULL_UP_DOWN_ENABLE_PORT_1 0x47
#define TCAL6416_CMD_PULL_UP_DOWN_SELECTION_PORT_0 0x48
#define TCAL6416_CMD_PULL_UP_DOWN_SELECTION_PORT_1 0x49
#define TCAL6416_CMD_INTERRUPT_MASK_PORT_0 0x4A
#define TCAL6416_CMD_INTERRUPT_MASK_PORT_1 0x4B
#define TCAL6416_CMD_INTERRUPT_STATUS_PORT_0 0x4C
#define TCAL6416_CMD_INTERRUPT_STATUS_PORT_1 0x4D
#define TCAL6416_CMD_OUTPUT_PORT_CONFIG 0x4F

static int tcal6416_read_reg16(gpiochip* chip, uint8_t base_reg, u_int16_t* result) {

    int ret;
    uint8_t buf[2];

    tcal6416* drvdata = (tcal6416*)chip->drvdata;

    ret = qi2c_master_write_read(drvdata->bus, drvdata->i2c_addr, &base_reg, 1, buf, 2);
    if (ret != 0) {
        return ret;
    }

    *result = (buf[1] << 8) | buf[0];

    return 0;

}

static int tcal6416_write_reg16(gpiochip* chip, uint8_t base_reg, u_int16_t value) {

    uint8_t buf[3];

    tcal6416* drvdata = (tcal6416*)chip->drvdata;

    buf[0] = base_reg;
    buf[1] = value & 0xFF;
    buf[2] = (value >> 8) & 0xFF;

    return qi2c_master_write(drvdata->bus, drvdata->i2c_addr, buf, 3);

}

// static int tcal6416_read_reg8(gpiochip* chip, uint8_t base_reg, uint8_t* result) {

//     int ret;
//     uint8_t buf[1];
//     tcal6416* drvdata = (tcal6416*)chip->drvdata;

//     ret = qi2c_master_write_read(drvdata->bus, drvdata->i2c_addr, &base_reg, 1, buf, 1);
//     if (ret != 0) {
//         return ret;
//     }
//     *result = buf[0];
//     return 0;

// }

static int tcal6416_write_reg8(gpiochip* chip, uint8_t base_reg, uint8_t value) {

    uint8_t buf[2];

    tcal6416* drvdata = (tcal6416*)chip->drvdata;

    buf[0] = base_reg;
    buf[1] = value;

    return qi2c_master_write(drvdata->bus, drvdata->i2c_addr, buf, 2);

}

static int tcal6416_write_regmap(gpiochip* chip) {

    int ret;
    uint8_t buf[14];
    tcal6416* drvdata = (tcal6416*)chip->drvdata;
    tcal6416_regmap* regmap = drvdata->regmap;

    buf[0] = TCAL6416_CMD_OUTPUT_PORT_0;
    buf[1] = regmap->output_port & 0xFF;
    buf[2] = (regmap->output_port >> 8) & 0xFF;

    buf[3] = regmap->polarity_inversion & 0xFF;
    buf[4] = (regmap->polarity_inversion >> 8) & 0xFF;

    buf[5] = regmap->configuration & 0xFF;
    buf[6] = (regmap->configuration >> 8) & 0xFF;

    // reg addr jumps here so write buf
    ret = qi2c_master_write(drvdata->bus, drvdata->i2c_addr, buf, 7);
    if (ret != 0) {
        return ret;
    }

    buf[0] = TCAL6416_CMD_OUTPUT_DRIVE_STRNGTH_PORT_0_0;

    buf[1] = regmap->output_drive_strength_0 & 0xFF;
    buf[2] = (regmap->output_drive_strength_0 >> 8) & 0xFF;
    buf[3] = regmap->output_drive_strength_1 & 0xFF;
    buf[4] = (regmap->output_drive_strength_1 >> 8) & 0xFF;

    buf[5] = regmap->input_latch & 0xFF;
    buf[6] = (regmap->input_latch >> 8) & 0xFF;

    buf[7] = regmap->pull_up_down_enable & 0xFF;
    buf[8] = (regmap->pull_up_down_enable >> 8) & 0xFF;

    buf[9] = regmap->pull_up_down_select & 0xFF;
    buf[10] = (regmap->pull_up_down_select >> 8) & 0xFF;

    buf[11] = regmap->interrupt_mask & 0xFF;
    buf[12] = (regmap->interrupt_mask >> 8) & 0xFF;

    buf[13] = regmap->output_port_config & 0xFF;

    return qi2c_master_write(drvdata->bus, drvdata->i2c_addr, buf, 14);
}

static int tcal6416_set_output_port_cfg(gpiochip* chip) {
        tcal6416* drvdata = (tcal6416*)chip->drvdata;

        drvdata->regmap->output_port_config = (drvdata->p1x_ode << 1) | drvdata->p0x_ode;
        return tcal6416_write_reg8(chip, TCAL6416_CMD_OUTPUT_PORT_CONFIG, drvdata->regmap->output_port_config);
}

static int tcal6416_unmask_irq(gpiochip* chip, int pin) {

        tcal6416* drvdata = (tcal6416*)chip->drvdata;

        drvdata->regmap->interrupt_mask &= ~(1 << pin);

        return tcal6416_write_reg16(chip, TCAL6416_CMD_INTERRUPT_MASK_PORT_0, drvdata->regmap->interrupt_mask);

}

static int tcal6416_mask_irq(gpiochip* chip, int pin) {

        tcal6416* drvdata = (tcal6416*)chip->drvdata;

        drvdata->regmap->interrupt_mask |= (1 << pin);

        return tcal6416_write_reg16(chip, TCAL6416_CMD_INTERRUPT_MASK_PORT_0, drvdata->regmap->interrupt_mask);

}

static int tcal6416_isr_add(gpiochip* chip, int pin, enum gpio_edge edge, void (*isr)(void*), void* data) {

    int ret;
    tcal6416* drvdata = (tcal6416*)chip->drvdata;

    if (drvdata->irq_info->irq_hooks[pin] != NULL) {
        // ISR already registered
        return -EADDRINUSE;
    }

    tcal6416_read_reg16(chip, TCAL6416_CMD_INPUT_PORT_0, &drvdata->irq_info->last_input_state);

    drvdata->irq_info->irq_hooks[pin] = isr;
    drvdata->irq_info->irq_data[pin] = data;
    drvdata->irq_info->irq_edges[pin] = edge;

    ret = tcal6416_unmask_irq(chip, pin);
    if (ret != 0) {
        return ret;
    }

    return 0;

}

static int tcal6416_isr_remove(gpiochip* chip, int pin) {

    int ret;

    tcal6416* drvdata = (tcal6416*)chip->drvdata;

    if (drvdata->irq_info->irq_hooks[pin] == NULL) {
        // ISR not registered
        return -EINVAL;
    }

    ret = tcal6416_mask_irq(chip, pin);
    if (ret != 0) {
        return ret;
    }

    drvdata->irq_info->irq_hooks[pin] = NULL;
    drvdata->irq_info->irq_data[pin] = NULL;
    drvdata->irq_info->irq_edges[pin] = GPIO_EDGE_NONE;

    return 0;

}

static IRAM_ATTR void tcal6416_isr_handler(void* arg) {

    gpiochip* chip = (gpiochip*)arg;
    tcal6416* drvdata = (tcal6416*)chip->drvdata;

    tcal6416_read_reg16(chip, TCAL6416_CMD_INTERRUPT_STATUS_PORT_0, &drvdata->regmap->interrupt_status);

    for (int i = 0; i < 16; i++) {
        // check if line fired interrupt
        if (((drvdata->regmap->interrupt_status >> i) & 0x01) == 1) {

            // check irq edge
            if (drvdata->irq_info->irq_edges[i] == GPIO_EDGE_FALLING && ((drvdata->irq_info->last_input_state >> i) & 0x01) == 0) {
                continue;
            }
            if (drvdata->irq_info->irq_edges[i] == GPIO_EDGE_RISING && ((drvdata->irq_info->last_input_state >> i) & 0x01) == 1) {
                continue;
            }
            if (drvdata->irq_info->irq_edges[i] == GPIO_EDGE_NONE) {
                continue;
            }

            // toggle last irq pin state
            drvdata->irq_info->last_input_state ^= (1 << i);

            // check if isr is registered
            if (drvdata->irq_info->irq_hooks[i] != NULL) {
                drvdata->irq_info->irq_hooks[i](drvdata->irq_info->irq_data[i]);
            } else {
                puts("tcal6416: unknown interrupt");
                abort();
            }
        }
    }

}

static int tcal6416_set_pin(gpiochip* chip, int pin, int value) {

    tcal6416* drvdata = (tcal6416*)chip->drvdata;

    if (((drvdata->regmap->output_port >> pin) & 0x01) == value) {
        // pin already set to value
        return 0;
    }

    // toggle pin state
    drvdata->regmap->output_port ^= (1 << pin);

    return tcal6416_write_reg16(chip, TCAL6416_CMD_OUTPUT_PORT_0, drvdata->regmap->output_port);

}

static int tcal6416_get_pin(gpiochip* chip, int* result, int pin) {

    int ret;
    tcal6416* drvdata = (tcal6416*)chip->drvdata;

    ret = tcal6416_read_reg16(chip, TCAL6416_CMD_INPUT_PORT_0, &drvdata->regmap->input_port);
    if (ret != 0) {
        return ret;
    }

    *result = (drvdata->regmap->input_port >> pin) & 0x01;

    return 0;

}

static int tcal6416_set_direction(gpiochip* chip, int pin, enum gpio_direction direction) {

    int dir_value;
    tcal6416* drvdata = (tcal6416*)chip->drvdata;

    dir_value = (direction == GPIO_DIRECTION_OUT) ? 0 : 1;
    if (((drvdata->regmap->configuration >> pin) & 0x01) == dir_value) {
        // direction already set correctly
        return 0;
    }

    // toggle direction state
    drvdata->regmap->configuration ^= (1 << pin);
    return tcal6416_write_reg16(chip, TCAL6416_CMD_CONFIGURATION_PORT_0, drvdata->regmap->configuration);

}

static int tcal6416_get_direction(gpiochip* chip, enum gpio_direction* result, int pin) {

    int ret;
    tcal6416* drvdata = (tcal6416*)chip->drvdata;

    ret = tcal6416_read_reg16(chip, TCAL6416_CMD_CONFIGURATION_PORT_0, &drvdata->regmap->configuration);
    if (ret != 0) {
        return ret;
    }
    *result = (drvdata->regmap->configuration >> pin) & 0x01 ? GPIO_DIRECTION_IN : GPIO_DIRECTION_OUT;
    return 0;
}

static uint8_t tcal6416_get_drive_value(enum tcal6416_drive drive) {

    switch (drive) {
        case TCAL6416_DRIVE_025:
            return 0x00;
        case TCAL6416_DRIVE_050:
            return 0x01;
        case TCAL6416_DRIVE_075:
            return 0x02;
        case TCAL6416_DRIVE_100:
            return 0x03;
        default:
            return 0x00;
    }

}

static void tcal6416_set_drive(gpiochip* chip, uint16_t pin_mask, enum tcal6416_drive drive) {

    tcal6416_regmap* regmap = ((tcal6416*)chip->drvdata)->regmap;
    uint8_t drive_value = tcal6416_get_drive_value(drive);

    for (int pin = 0; pin < TCAL6416_NGPIO; pin++) {
        if ((pin_mask >> pin) & 0x01) {
            if (pin < 8) {
                regmap->output_drive_strength_0 &= ~(0x03 << (pin * 2));
                regmap->output_drive_strength_0 |= (drive_value << (pin * 2));
            } else {
                regmap->output_drive_strength_1 &= ~(0x03 << ((pin - 8) * 2));
                regmap->output_drive_strength_1 |= (drive_value << ((pin - 8) * 2));
            }
        }
    }

}

int tcal6416_init_pins(gpiochip* chip, tcal6416_pins* pins) {

    tcal6416* drvdata = (tcal6416*)chip->drvdata;
    tcal6416_regmap* regmap = drvdata->regmap;

    if (pins->direction == GPIO_DIRECTION_IN) {
        regmap->configuration = set_bits16_value(regmap->configuration, pins->pin_mask, 1);
    } else {
        regmap->configuration = set_bits16_value(regmap->configuration, pins->pin_mask, 0);
    }

    if (pins->polarity == TCAL6416_INVERTED) {
        regmap->polarity_inversion = set_bits16_value(regmap->polarity_inversion, pins->pin_mask, 1);
    } else {
        regmap->polarity_inversion = set_bits16_value(regmap->polarity_inversion, pins->pin_mask, 0);
    }

    if (pins->pull == TCAL6416_PULL_NONE) {
        regmap->pull_up_down_enable = set_bits16_value(regmap->pull_up_down_enable, pins->pin_mask, 0);
    } else {
        regmap->pull_up_down_enable = set_bits16_value(regmap->pull_up_down_enable, pins->pin_mask, 1);
    }
    if (pins->pull == TCAL6416_PULL_UP) {
        regmap->pull_up_down_select = set_bits16_value(regmap->pull_up_down_select, pins->pin_mask, 1);
    } else {
        regmap->pull_up_down_select = set_bits16_value(regmap->pull_up_down_select, pins->pin_mask, 0);
    }

    tcal6416_set_drive(chip, pins->pin_mask, pins->drive);

    if (pins->irq_latch == TCAL6416_IRQ_LATCH) {
        regmap->input_latch = set_bits16_value(regmap->input_latch, pins->pin_mask, 1);
    } else {
        regmap->input_latch = set_bits16_value(regmap->input_latch, pins->pin_mask, 0);
    }

    regmap->output_port = set_bits16_value(regmap->output_port, pins->pin_mask, pins->inital_value);

    return tcal6416_write_regmap(chip);
}

int tcal6416_init(gpiochip* chip, tcal6416* config) {

    int ret;
    tcal6416_regmap* regmap = malloc(sizeof(tcal6416_regmap));
    tcal6416_irq_info* irq_info = calloc(1, sizeof(tcal6416_irq_info));
    tcal6416* drvdata = malloc(sizeof(tcal6416));
    memcpy(drvdata, config, sizeof(tcal6416));

    // setup regmap to reset values
    regmap->output_port = 0xFF;
    regmap->polarity_inversion = 0x00;
    regmap->configuration = 0x00;
    regmap->output_drive_strength_0 = 0xFF;
    regmap->output_drive_strength_1 = 0xFF;
    regmap->input_latch = 0x00;
    regmap->pull_up_down_enable = 0x00;
    regmap->pull_up_down_select = 0xFF;
    regmap->interrupt_mask = 0xFF;
    regmap->interrupt_status = 0x00;
    regmap->output_port_config = 0x00;

    config->irq_info = irq_info;
    config->regmap = regmap;

    chip->drvdata = config;
    chip->ngpio = TCAL6416_NGPIO;
    chip->get = tcal6416_get_pin;
    chip->set = tcal6416_set_pin;
    chip->set_direction = tcal6416_set_direction;
    chip->get_direction = tcal6416_get_direction;
    chip->register_isr = tcal6416_isr_add;
    chip->unregister_isr = tcal6416_isr_remove;

    // reset chip
    qgpio_set(config->reset_chip, config->reset_pin, 0);
    vTaskDelay(pdMS_TO_TICKS(1));
    qgpio_set(config->reset_chip, config->reset_pin, 1);
    vTaskDelay(pdMS_TO_TICKS(1));

    // set ports to pushpull or open drain
    ret = tcal6416_set_output_port_cfg(chip);
    if (ret != 0) {
        return ret;
    }

    // read in current input states for irq edge detection
    ret = tcal6416_read_reg16(chip, TCAL6416_CMD_INPUT_PORT_0, &regmap->input_port);
    if (ret != 0) {
        return ret;
    }
    irq_info->last_input_state = regmap->input_port;

    ret = qgpio_register_isr(config->irq_chip, config->irq_pin, GPIO_EDGE_FALLING, tcal6416_isr_handler, (void*)chip);
    if (ret != 0) {
        return ret;
    }

    return 0;

}

int tcal6416_deinit(gpiochip* chip) {

    tcal6416* drvdata = (tcal6416*)chip->drvdata;

    free(drvdata->regmap);
    free(drvdata->irq_info);
    free(drvdata);

    return 0;

}