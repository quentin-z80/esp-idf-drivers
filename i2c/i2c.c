#include <stdint.h>
#include <stddef.h>

#include "i2c.h"

int qi2c_master_read(i2c_bus* bus, uint8_t addr, uint8_t* data, size_t len) {
    return bus->master_read(bus, addr, data, len);
}

int qi2c_master_write(i2c_bus* bus, uint8_t addr, uint8_t* data, size_t len) {
    return bus->master_write(bus, addr, data, len);
}

int qi2c_master_write_read(i2c_bus* bus, uint8_t addr, uint8_t* write_buf, size_t write_len, uint8_t* read_buf, size_t read_len) {
    return bus->master_write_read(bus, addr, write_buf, write_len, read_buf, read_len);
}
