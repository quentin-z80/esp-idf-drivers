#pragma once

#include "i2c.h"

#define BQ21080_DEVICE_ID 0x00

typedef union bq21080_stat0 {
    struct {
        uint8_t vin_pgood_stat : 1;
        uint8_t thermreg_active_stat : 1;
        uint8_t vindpm_active_stat : 1;
        uint8_t vdppm_active_stat : 1;
        uint8_t ilim_active_stat: 1;
        uint8_t chg_stat : 2;
        uint8_t ts_open_stat : 1;
    } fields;
    uint8_t reg;
} bq21080_stat0;

typedef union bq21080_stat1 {
    struct {
        uint8_t wake2_flag : 1;
        uint8_t wake1_flag : 1;
        uint8_t safety_tmr_fault_flag : 2;
        uint8_t ts_stat : 2;
        uint8_t reserved : 1;
        uint8_t buvlo_stat : 1;
        uint8_t vin_ovp_stat : 1;
    } fields;
    uint8_t reg;
} bq21080_stat1;

typedef union bq21080_flag0 {
    struct {
        uint8_t bat_ocp_fault : 1;
        uint8_t buvlo_fault_flag : 1;
        uint8_t vin_ovp_fault_flag : 1;
        uint8_t thermreg_active_flag : 1;
        uint8_t vindpm_active_flag : 1;
        uint8_t vdppm_active_flag : 1;
        uint8_t ilim_active_flag : 1;
        uint8_t ts_fault : 1;
    } fields;
    uint8_t reg;
} bq21080_flag0;

typedef union bq21080_vbat_ctrl {
    struct {
        uint8_t vbatreg : 7;
        uint8_t reserved : 1;
    } fields;
    uint8_t reg;
} bq21080_vbat_ctrl;

typedef union bq21080_ichg_ctrl {
    struct {
        uint8_t ichgreg : 7;
        uint8_t chg_dis : 1;
    } fields;
    uint8_t reg;
} bq21080_ichg_ctrl;

typedef union bq21080_chargectrl0 {
    struct {
        uint8_t therm_reg : 2;
        uint8_t vindpm : 2;
        uint8_t iterm : 2;
        uint8_t iprechg : 1;
        uint8_t reserved : 1;
    } fields;
    uint8_t reg;
} bq21080_chargectrl0;

typedef union bq21080_chargectrl1 {
    struct {
        uint8_t vdpm_int_mask : 1;
        uint8_t ilim_int_mask : 1;
        uint8_t chg_status_int_mask : 1;
        uint8_t bulvo : 3;
        uint8_t ibat_ocp : 2;
    } fields;
    uint8_t reg;
} bq21080_chargectrl1;

typedef union bq21080_ic_ctrl {
    struct {
        uint8_t watchdog_sel : 2;
        uint8_t safety_timer : 2;
        uint8_t tmr2x_en : 1;
        uint8_t vrch_0 : 1;
        uint8_t vlowv_sel : 1;
        uint8_t ts_en : 1;
    } fields;
    uint8_t reg;
} bq21080_ic_ctrl;

typedef union bq21080_tmr_ilim {
    struct {
        uint8_t ilim : 3;
        uint8_t autowake : 2;
        uint8_t mr_reset_vin : 1;
        uint8_t mr_lpress : 2;
    } fields;
    uint8_t reg;
} bq21080_tmr_ilim;

typedef union bq21080_ship_rst {
    struct {
        uint8_t en_push : 1;
        uint8_t wake2_tmr : 1;
        uint8_t wake1_tmr : 1;
        uint8_t pb_lrpress_action : 2;
        uint8_t en_rst_ship : 2;
        uint8_t reg_rst : 1;
    } fields;
    uint8_t reg;
} bq21080_ship_rst;

typedef union bq21080_sys_reg {
    struct {
        uint8_t wdppm_dis : 1;
        uint8_t watchdog_15s_enable : 1;
        uint8_t sys_mode : 2;
        uint8_t reserved : 1;
        uint8_t sys_reg_ctrl : 3;
    } fields;
    uint8_t reg;
} bq21080_sys_reg;

typedef union bq21080_ts_control {
    struct {
        uint8_t ts_vrcg : 1;
        uint8_t ts_ichg : 1;
        uint8_t ts_cool : 1;
        uint8_t ts_warm : 1;
        uint8_t ts_cold : 2;
        uint8_t ts_hot : 2;
    } fields;
    uint8_t reg;
} bq21080_ts_control;

typedef union bq21080_mask_id {
    struct {
        uint8_t device_id : 4;
        uint8_t pg_int_mask : 1;
        uint8_t bat_int_mask : 1;
        uint8_t treg_int_mask : 1;
        uint8_t ts_int_mask : 1;
    } fields;
    uint8_t reg;
} bq21080_mask_id;

typedef struct bq21080 {
    i2c_bus* bus;
    uint8_t addr;
} bq21080;

int bq21080_read_id(bq21080* chip, uint8_t* id);
/* dump bq21080 register data to string buffer. */
int bq21080_debug_dump(bq21080* chip, char* output);
int bq21080_set_charge_current(bq21080* chip, int milliamps);
/* sets the mimimum input voltage before reducing charge current */
int bq21080_set_power_point(bq21080* chip, int millivolts);
/* enter ship mode with wake on button press or adapter insert */
int bq21080_ship(bq21080* chip);
/* enter shutdown mode with wake on adapter insert */
int bq21080_shutdown(bq21080* chip);
int bq21080_set_vsys_voltage(bq21080* chip, int millivolts);
int bq21080_reset(bq21080* chip);

int bq21080_init(bq21080* chip, i2c_bus* bus, uint8_t addr);
