#include <errno.h>

#include "esp_err.h"

int err_esp_to_errno(esp_err_t err) {
    switch (err) {
        case ESP_OK:
            return 0;
        case ESP_FAIL:
            return -EFAULT;
        case ESP_ERR_NO_MEM:
            return -ENOMEM;
        case ESP_ERR_INVALID_ARG:
            return -EINVAL;
        case ESP_ERR_INVALID_STATE:
            return -EINVAL;
        case ESP_ERR_INVALID_SIZE:
            return -EINVAL;
        case ESP_ERR_NOT_FOUND:
            return -ENOENT;
        case ESP_ERR_NOT_SUPPORTED:
            return -ENOTSUP;
        case ESP_ERR_TIMEOUT:
            return -ETIMEDOUT;
        case ESP_ERR_INVALID_RESPONSE:
            return -EINVAL;
        case ESP_ERR_INVALID_CRC:
            return -EINVAL;
        case ESP_ERR_INVALID_VERSION:
            return -EINVAL;
        case ESP_ERR_INVALID_MAC:
            return -EINVAL;
        case ESP_ERR_NOT_FINISHED:
            return -EAGAIN;
        case ESP_ERR_WIFI_BASE:
            return -EIO;
        case ESP_ERR_MESH_BASE:
            return -EIO;
        case ESP_ERR_FLASH_BASE:
            return -EIO;
        case ESP_ERR_HW_CRYPTO_BASE:
            return -EIO;
        case ESP_ERR_MEMPROT_BASE:
            return -EIO;
        default:
            return -ENOTSUP;
    }
}
