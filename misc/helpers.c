#include <stdint.h>

#include "esp_timer.h"

#include "helpers.h"

uint16_t set_bits16_value(uint16_t number, uint16_t mask, uint16_t value) {
    // mask != 0
    number &= ~mask;
    while (!(mask & 1)) {
        value <<= 1;
        mask >>= 1;
    }
    return number | value;
}

void udelay(int delay_us) {
    int starttime_us = esp_timer_get_time();

    if (delay_us != 0) {
        while ((esp_timer_get_time() - starttime_us) <= delay_us) {
            // busy wait
        }
    }
}