#include <errno.h>

#include "gpio.h"

int qgpio_get(gpiochip* chip, int* result, int pin) {
    return chip->get(chip, result, pin);
}

int qgpio_set(gpiochip* chip, int pin, int value) {
    return chip->set(chip, pin, value);
}

int qgpio_set_direction(gpiochip* chip, int pin, enum gpio_direction direction) {
    return chip->set_direction(chip, pin, direction);
}

int qgpio_get_direction(gpiochip* chip, enum gpio_direction* result, int pin) {
    return chip->get_direction(chip, result, pin);
}

int qgpio_register_isr(gpiochip* chip, int pin, enum gpio_edge edge, void (*isr)(void*), void* data) {
    if (!chip->register_isr) {
        return -ENOTSUP;
    }
    return chip->register_isr(chip, pin, edge, isr, data);
}

int qgpio_unregister_isr(gpiochip* chip, int pin) {
    if (!chip->unregister_isr) {
        return -ENOTSUP;
    }
    return chip->unregister_isr(chip, pin);
}
