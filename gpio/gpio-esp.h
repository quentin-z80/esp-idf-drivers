#pragma once

#include "driver/gpio.h"

#include "gpio.h"

int gpio_esp_init(gpiochip* chip);
int gpio_esp_init_pins(gpiochip* chip, gpio_config_t* pins);
