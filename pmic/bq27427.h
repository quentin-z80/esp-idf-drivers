#include "i2c.h"
#include "gpio.h"

#pragma once

typedef struct bq27427 {
    i2c_bus* bus;
    uint8_t address;
} bq27427;

typedef struct bq27427_config {
    i2c_bus* bus;
    uint8_t address;
    int design_capacity;
} bq27427_config;

int bq27427_get_chem_id(bq27427* chip, uint16_t* chem_id);
int bq27427_get_device_type(bq27427* chip, uint16_t* device_type);
int bq27427_get_voltage(bq27427* chip, uint16_t* voltage);
int bq27427_get_power(bq27427* chip, int16_t* power);
int bq27427_get_current(bq27427* chip, int16_t* current);
int bq27427_get_soc(bq27427* chip, uint16_t* soc);
int bq27427_get_nom_cap(bq27427* chip, uint16_t* nom_cap);
int bq27427_get_full_cap(bq27427* chip, uint16_t* full_cap);
int bq27427_get_remain_cap(bq27427* chip, uint16_t* remain_cap);

int bq27427_init(bq27427* chip, bq27427_config* config);