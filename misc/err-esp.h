#pragma once

#include <esp_err.h>

int err_esp_to_errno(esp_err_t err);