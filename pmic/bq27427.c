#include <errno.h>

#include "esp_log.h"

#include "i2c.h"
#include "gpio.h"

#include "bq27427.h"

#define BQ27427_ID 0x0427

#define BQ27427_REG_CONTROL 0x00
#define BQ27427_REG_TEMP 0x02
#define BQ27427_REG_VOLTAGE 0x04
#define BQ27427_REG_FLAGS 0x06
#define BQ27427_REG_NOM_AVAIL_CAP 0x08
#define BQ27427_REG_FULL_AVAIL_CAP 0x0A
#define BQ27427_REG_REMAIN_CAP 0x0C
#define BQ27427_REG_FULL_CHARGE_CAP 0x0E
#define BQ27427_REG_AVG_CURRENT 0x10
#define BQ27427_REG_AVG_POWER 0x18
#define BQ27427_REG_SOC 0x1C
#define BQ27427_REG_INT_TEMP 0x1E
#define BQ27427_REG_REMAIN_CAP_UNFL 0x28
#define BQ27427_REG_REMAIN_CAP_FIL 0x2A
#define BQ27427_REG_FULL_CAP_UNFL 0x2C
#define BQ27427_REG_FULL_CAP_FIL 0x2E
#define BQ27427_REG_SOC_UNFL 0x30

#define BQ27427_CTRL_STATUS 0x0000
#define BQ27427_CTRL_DEVICE_TYPE 0x0001
#define BQ27427_CTRL_FW_VERSION 0x0002
#define BQ27427_CTRL_DM_CODE 0x0004
#define BQ27427_CTRL_PREV_MACWRITE 0x0007
#define BQ27427_CTRL_CHEM_ID 0x0008
#define BQ27427_CTRL_BAT_INSERT 0x000C
#define BQ27427_CTRL_BAT_REMOVE 0x000D
#define BQ27427_CTRL_SET_CFGUPDATE 0x0013
#define BQ27427_CTRL_SMOOTH_SYNC 0x0019
#define BQ27427_CTRL_SHUTDOWN_ENABLE 0x001B
#define BQ27427_CTRL_SHUTDOWN 0x001C
#define BQ27427_CTRL_SEALED 0x0020
#define BQ27427_CTRL_PULSE_SOC_INT 0x0023
#define BQ27427_CTRL_CHEM_A 0x0030
#define BQ27427_CTRL_CHEM_B 0x0031
#define BQ27427_CTRL_CHEM_C 0x0032
#define BQ27427_CTRL_RESET 0x0041
#define BQ27427_CTRL_SOFT_RESET 0x0042

static int bq27427_ctrl_reg_write(bq27427* chip, int16_t subcmd) {

    uint8_t buf[3] = {
        BQ27427_REG_CONTROL,
        subcmd & 0xFF,
        (subcmd >> 8) & 0xFF
    };

    return qi2c_master_write(chip->bus, chip->address, buf, sizeof(buf));

}

static int bq27427_cmd_reg_read(bq27427* chip, uint8_t reg, uint16_t* val) {

    int ret;
    uint8_t buf[2];

    ret = qi2c_master_write_read(chip->bus, chip->address, &reg, sizeof(reg), buf, sizeof(buf));

    ESP_LOGI("bq27427", "read reg 0x%02x: 0x%02x 0x%02x", reg, buf[0], buf[1]);

    if (ret != 0) {
        return ret;
    }

    *val = buf[0] | (buf[1] << 8);

    return 0;

}

int bq27427_get_chem_id(bq27427* chip, uint16_t* chem_id) {

        int ret;

        ret = bq27427_ctrl_reg_write(chip, BQ27427_CTRL_CHEM_ID);
        if (ret != 0) {
            return ret;
        }

        return bq27427_cmd_reg_read(chip, BQ27427_REG_CONTROL, chem_id);
}

int bq27427_get_device_type(bq27427* chip, uint16_t* device_type) {

    int ret;

    ret = bq27427_ctrl_reg_write(chip, BQ27427_CTRL_DEVICE_TYPE);
    if (ret != 0) {
        return ret;
    }

    return bq27427_cmd_reg_read(chip, BQ27427_REG_CONTROL, device_type);

}

int bq27427_get_voltage(bq27427* chip, uint16_t* voltage) {
    return bq27427_cmd_reg_read(chip, BQ27427_REG_VOLTAGE, voltage);
}

int bq27427_get_power(bq27427* chip, int16_t* power) {
    return bq27427_cmd_reg_read(chip, BQ27427_REG_AVG_POWER, (uint16_t*)power);
}

int bq27427_get_current(bq27427* chip, int16_t* current) {
    return bq27427_cmd_reg_read(chip, BQ27427_REG_AVG_CURRENT, (uint16_t*)current);
}

int bq27427_get_soc(bq27427* chip, uint16_t* soc) {
    return bq27427_cmd_reg_read(chip, BQ27427_REG_SOC, soc);
}

int bq27427_get_nom_cap(bq27427* chip, uint16_t* nom_cap) {
    return bq27427_cmd_reg_read(chip, BQ27427_REG_NOM_AVAIL_CAP, nom_cap);
}

int bq27427_get_full_cap(bq27427* chip, uint16_t* full_cap) {
    return bq27427_cmd_reg_read(chip, BQ27427_REG_FULL_AVAIL_CAP, full_cap);
}

int bq27427_get_remain_cap(bq27427* chip, uint16_t* remain_cap) {
    return bq27427_cmd_reg_read(chip, BQ27427_REG_REMAIN_CAP, remain_cap);
}

int bq27427_init(bq27427* chip, bq27427_config* config) {

    int ret;
    uint16_t device_type;

    chip->bus = config->bus;
    chip->address = config->address;

    ret = bq27427_get_device_type(chip, &device_type);
    if (ret != 0) {
        return ret;
    }

    if (device_type != BQ27427_ID) {
        return -ENOTSUP;
    }

    return 0;

}
