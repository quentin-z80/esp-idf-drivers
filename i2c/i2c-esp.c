#include <errno.h>

#include "esp_err.h"
#include "driver/i2c.h"

#include "err-esp.h"
#include "i2c.h"

#include "i2c-esp.h"

static int i2c_esp_master_write(i2c_bus* bus, uint8_t addr, uint8_t* data, size_t len) {
    esp_err_t ret;
    i2c_esp_bus* drvdata = (i2c_esp_bus*)bus->drvdata;
    ret = i2c_master_write_to_device(drvdata->port, addr >> 1, data, len, drvdata->timeout);
    return err_esp_to_errno(ret);
}

static int i2c_esp_master_read(i2c_bus* bus, uint8_t addr, uint8_t* data, size_t len) {
    esp_err_t ret;
    i2c_esp_bus* drvdata = (i2c_esp_bus*)bus->drvdata;
    ret = i2c_master_read_from_device(drvdata->port, addr >> 1, data, len, drvdata->timeout);
    return err_esp_to_errno(ret);
}

static int i2c_esp_master_write_read(i2c_bus* bus, uint8_t addr, uint8_t* write_buf, size_t write_len, uint8_t* read_buf, size_t read_len) {
    esp_err_t ret;
    i2c_esp_bus* drvdata = (i2c_esp_bus*)bus->drvdata;
    ret = i2c_master_write_read_device(drvdata->port, addr >> 1, write_buf, write_len, read_buf, read_len, drvdata->timeout);
    return err_esp_to_errno(ret);
}

int i2c_esp_init(i2c_bus* bus, i2c_config_t *config, i2c_port_t port, i2c_mode_t mode, unsigned int timeout) {

    esp_err_t ret;
    i2c_esp_bus* drvdata = malloc(sizeof(i2c_esp_bus));

    drvdata->port = port;
    drvdata->config = config;
    drvdata->timeout = timeout;
    bus->drvdata = drvdata;

    ret = i2c_param_config(port, config);
    if (ret != ESP_OK) {
        return err_esp_to_errno(ret);
    }
    ret = i2c_driver_install(port, mode, 0, 0, 0);
    if (ret != ESP_OK) {
        return err_esp_to_errno(ret);
    }

    if (mode == I2C_MODE_SLAVE) {
        return -ENOTSUP;
    }

    bus->master_write = i2c_esp_master_write;
    bus->master_read = i2c_esp_master_read;
    bus->master_write_read = i2c_esp_master_write_read;

    return 0;

}

int i2c_esp_deinit(i2c_bus* bus) {

    esp_err_t ret;
    i2c_esp_bus* drvdata = (i2c_esp_bus*)bus->drvdata;

    ret = i2c_driver_delete(drvdata->port);
    if (ret != ESP_OK) {
        return err_esp_to_errno(ret);
    }

    free(drvdata);

    return 0;

}
