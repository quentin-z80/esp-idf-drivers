#pragma once

#include <stdint.h>
#include <stddef.h>

typedef struct i2c_bus {
    void* drvdata;
    int (*master_write)(struct i2c_bus* bus, uint8_t addr, uint8_t* data, size_t len);
    int (*master_read)(struct i2c_bus* bus, uint8_t addr, uint8_t* data, size_t len);
    int (*master_write_read)(struct i2c_bus* bus, uint8_t addr, uint8_t* write_buf, size_t write_len, uint8_t* read_buf, size_t read_len);
} i2c_bus;

int qi2c_master_read(i2c_bus* bus, uint8_t addr, uint8_t* data, size_t len);
int qi2c_master_write(i2c_bus* bus, uint8_t addr, uint8_t* data, size_t len);
int qi2c_master_write_read(i2c_bus* bus, uint8_t addr, uint8_t* write_buf, size_t write_len, uint8_t* read_buf, size_t read_len);