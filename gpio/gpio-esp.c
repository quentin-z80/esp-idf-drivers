#include <errno.h>

#include "driver/gpio.h"
#include "esp_err.h"

#include "err-esp.h"

#include "gpio-esp.h"

static gpio_int_type_t gpio_edge_to_esp_edge(enum gpio_edge edge) {
    switch (edge) {
        case GPIO_EDGE_NONE:
            return GPIO_INTR_DISABLE;
        case GPIO_EDGE_RISING:
            return GPIO_INTR_POSEDGE;
        case GPIO_EDGE_FALLING:
            return GPIO_INTR_NEGEDGE;
        case GPIO_EDGE_BOTH:
            return GPIO_INTR_ANYEDGE;
        default:
            return GPIO_INTR_DISABLE;
    }
}

static int gpio_esp_get(gpiochip* chip, int* result, int pin) {

    *result = gpio_get_level(pin);
    return 0;
}

static int gpio_esp_set(gpiochip* chip, int pin, int value) {
    esp_err_t ret;
    ret = gpio_set_level(pin, value);
    return err_esp_to_errno(ret);
}

static int gpio_esp_set_direction(gpiochip* chip, int pin, enum gpio_direction direction) {
    esp_err_t ret;
    ret = gpio_set_direction(pin, direction);
    return err_esp_to_errno(ret);
}

static int gpio_esp_get_direction(gpiochip* chip, enum gpio_direction* result, int pin) {
    return -ENOTSUP;
}

static int gpio_esp_isr_add(gpiochip* chip, int pin, enum gpio_edge edge, void (*isr)(void*), void* data) {

    esp_err_t ret;
    gpio_int_type_t esp_edge;

    esp_edge = gpio_edge_to_esp_edge(edge);
    ret = gpio_set_intr_type(pin, esp_edge);

    ret = gpio_isr_handler_add(pin, isr, data);
    return err_esp_to_errno(ret);
}

static int gpio_esp_isr_remove(gpiochip* chip, int pin) {

    esp_err_t ret;

    ret = gpio_isr_handler_remove(pin);
    return err_esp_to_errno(ret);
}

int gpio_esp_init_pins(gpiochip* chip, gpio_config_t* pins) {
    esp_err_t ret;
    ret = gpio_config(pins);
    return err_esp_to_errno(ret);
}

int gpio_esp_init(gpiochip* chip) {

    chip->drvdata = NULL;
    chip->get = gpio_esp_get;
    chip->set = gpio_esp_set;
    chip->set_direction = gpio_esp_set_direction;
    chip->get_direction = gpio_esp_get_direction;
    chip->register_isr = gpio_esp_isr_add;
    chip->unregister_isr = gpio_esp_isr_remove;
    chip->ngpio = GPIO_PIN_COUNT;

    return err_esp_to_errno(gpio_install_isr_service(0));

}
