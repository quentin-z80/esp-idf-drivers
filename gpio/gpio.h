#pragma once

enum gpio_edge {
    GPIO_EDGE_NONE,
    GPIO_EDGE_RISING,
    GPIO_EDGE_FALLING,
    GPIO_EDGE_BOTH,
};

enum gpio_direction {
    GPIO_DIRECTION_IN,
    GPIO_DIRECTION_OUT,
};

typedef struct gpiochip {
    void* drvdata;
    int (*get)(struct gpiochip* chip, int* result, int pin);
    int (*set)(struct gpiochip* chip, int pin, int value);
    int (*set_direction)(struct gpiochip* chip, int pin, enum gpio_direction direction);
    int (*get_direction)(struct gpiochip* chip, enum gpio_direction* result, int pin);
    int (*register_isr)(struct gpiochip* chip, int pin, enum gpio_edge edge, void (*isr)(void*), void* data);
    int (*unregister_isr)(struct gpiochip* chip, int pin);
    int ngpio;
} gpiochip;

int qgpio_get(gpiochip* chip, int* result, int pin);
int qgpio_set(gpiochip* chip, int pin, int value);
int qgpio_set_direction(gpiochip* chip, int pin, enum gpio_direction direction);
int qgpio_get_direction(gpiochip* chip, enum gpio_direction* result, int pin);
int qgpio_register_isr(gpiochip* chip, int pin, enum gpio_edge edge, void (*isr)(void*), void* data);
int qgpio_unregister_isr(gpiochip* chip, int pin);
