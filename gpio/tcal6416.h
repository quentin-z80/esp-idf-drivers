#pragma once

#include <stdbool.h>
#include <driver/i2c.h>

#include "gpio.h"
#include "i2c.h"

#define TCAL6416_NGPIO 16

enum TCAL6416_PINS {
    TCAL6416_PIN_0 = 0,
    TCAL6416_PIN_1 = 1,
    TCAL6416_PIN_2 = 2,
    TCAL6416_PIN_3 = 3,
    TCAL6416_PIN_4 = 4,
    TCAL6416_PIN_5 = 5,
    TCAL6416_PIN_6 = 6,
    TCAL6416_PIN_7 = 7,
    TCAL6416_PIN_10 = 8,
    TCAL6416_PIN_11 = 9,
    TCAL6416_PIN_12 = 10,
    TCAL6416_PIN_13 = 11,
    TCAL6416_PIN_14 = 12,
    TCAL6416_PIN_15 = 13,
    TCAL6416_PIN_16 = 14,
    TCAL6416_PIN_17 = 15
};

enum tcal6416_polarity {
    TCAL6416_NORMAL = 0,
    TCAL6416_INVERTED = 1
};

enum tcal6416_pull {
    TCAL6416_PULL_NONE = 0,
    TCAL6416_PULL_UP = 1,
    TCAL6416_PULL_DOWN = 2
};

enum tcal6416_ode {
    TCAL6416_PUSH_PULL = 0,
    TCAL6416_OPEN_DRAIN = 1
};

enum tcal6416_drive {
    TCAL6416_DRIVE_025 = 0,
    TCAL6416_DRIVE_050 = 1,
    TCAL6416_DRIVE_075 = 2,
    TCAL6416_DRIVE_100 = 3
};

enum tcal6416_irq_latch {
    TCAL6416_IRQ_NO_LATCH = 0,
    TCAL6416_IRQ_LATCH = 1
};

typedef struct tcal6416_irq_info {
    void (*irq_hooks[TCAL6416_NGPIO])(void*);
    void* irq_data[TCAL6416_NGPIO];
    enum gpio_edge irq_edges[TCAL6416_NGPIO];
    uint16_t last_input_state;
} tcal6416_irq_info;

typedef struct tcal6416_regmap {
    uint16_t input_port;
    uint16_t output_port;
    uint16_t polarity_inversion;
    uint16_t configuration;
    uint16_t output_drive_strength_0;
    uint16_t output_drive_strength_1;
    uint16_t input_latch;
    uint16_t pull_up_down_enable;
    uint16_t pull_up_down_select;
    uint16_t interrupt_mask;
    uint16_t interrupt_status;
    uint8_t output_port_config;
} tcal6416_regmap;

typedef struct tcal6416_pins {
    uint16_t pin_mask;
    uint16_t inital_value;
    enum gpio_direction direction;
    enum tcal6416_polarity polarity;
    enum tcal6416_pull pull;
    enum tcal6416_drive drive;
    enum tcal6416_irq_latch irq_latch;
} tcal6416_pins;

typedef struct tcal6416 {
    // user settings
    i2c_bus* bus;
    uint8_t i2c_addr;
    gpiochip* irq_chip;
    gpiochip* reset_chip;
    int irq_pin;
    int reset_pin;
    enum tcal6416_ode p0x_ode;
    enum tcal6416_ode p1x_ode;
    // internal data
    tcal6416_regmap* regmap;
    tcal6416_irq_info* irq_info;
} tcal6416;

int tcal6416_init(gpiochip* chip, tcal6416* config);
int tcal6416_init_pins(gpiochip* chip, tcal6416_pins* pins);
int tcal6416_deinit(gpiochip* chip);