
#include <stdio.h>
#include <errno.h>

#include "i2c.h"

#include "bq21080.h"

#define BQ21080_REG_STAT0 0x0
#define BQ21080_REG_STAT1 0x1
#define BQ21080_REG_FLAG0 0x2
#define BQ21080_REG_VBAT_CTRL 0x3
#define BQ21080_REG_ICHG_CTRL 0x4
#define BQ21080_REG_CHARGECTRL0 0x5
#define BQ21080_REG_CHARGECTRL1 0x6
#define BQ21080_REG_IC_CTRL 0x7
#define BQ21080_REG_TMR_ILIM 0x8
#define BQ21080_REG_SHIP_RST 0x9
#define BQ21080_REG_SYS_REG 0xA
#define BQ21080_REG_TS_CONTROL 0xB
#define BQ21080_REG_MASK_ID 0xC

static int bq21080_i2c_write(bq21080* chip, uint8_t* buf, uint8_t len) {
    return qi2c_master_write(chip->bus, chip->addr, buf, len);
}

static int bq21080_i2c_write_read(bq21080* chip, uint8_t* write_buf, uint8_t write_len, uint8_t* read_buf, uint8_t read_len) {
    return qi2c_master_write_read(chip->bus, chip->addr, write_buf, write_len, read_buf, read_len);
}

static int bq21080_read_reg(bq21080* chip, uint8_t reg, uint8_t* data) {
    return bq21080_i2c_write_read(chip, &reg, 1, data, 1);
}

static int bq21080_write_reg(bq21080* chip, uint8_t reg, uint8_t data) {
    uint8_t buf[2] = {reg, data};
    return bq21080_i2c_write(chip, buf, 2);
}

int bq21080_read_id(bq21080* chip, uint8_t* id) {
    int ret;
    bq21080_mask_id mask_id;
    ret =  bq21080_read_reg(chip, BQ21080_REG_MASK_ID, &mask_id.reg);
    if (ret != 0) {
        return ret;
    }
    *id = mask_id.fields.device_id;
    return 0;
}

int bq21080_debug_dump(bq21080* chip, char* output) {
    int ret;
    uint8_t base_reg = 0x0;
    uint8_t data[13];
    ret = bq21080_i2c_write_read(chip, &base_reg, 1, data, 13);
    if (ret != 0) {
        return ret;
    }

    sprintf(output, "STAT0: 0x%02x\n"
                    "STAT1: 0x%02x\n"
                    "FLAG0: 0x%02x\n"
                    "VBAT_CTRL: 0x%02x\n"
                    "ICHG_CTRL: 0x%02x\n"
                    "CHARGECTRL0: 0x%02x\n"
                    "CHARGECTRL1: 0x%02x\n"
                    "IC_CTRL: 0x%02x\n"
                    "TMR_ILIM: 0x%02x\n"
                    "SHIP_RST: 0x%02x\n"
                    "SYS_REG: 0x%02x\n"
                    "TS_CONTROL: 0x%02x\n"
                    "MASK_ID: 0x%02x",
                    data[0], data[1], data[2], data[3], data[4],
                    data[5], data[6], data[7], data[8], data[9],
                    data[10], data[11], data[12]);

    return 0;
}

int bq21080_set_charge_current(bq21080* chip, int milliamps) {
    int ret;
    bq21080_ichg_ctrl ichg_ctrl;
    ret = bq21080_read_reg(chip, BQ21080_REG_ICHG_CTRL, &ichg_ctrl.reg);
    if (ret != 0) {
        return ret;
    }

    if (milliamps <= 35) {
        ichg_ctrl.fields.ichgreg = milliamps - 5;
    } else if (milliamps <= 800) {
        ichg_ctrl.fields.ichgreg = ((milliamps - 40) / 10) + 31;
    } else {
        return -EINVAL;
    }

    ichg_ctrl.fields.chg_dis = 0b0;

    return bq21080_write_reg(chip, BQ21080_REG_ICHG_CTRL, ichg_ctrl.reg);

}

int bq21080_set_power_point(bq21080* chip, int millivolts) {
    int ret;
    bq21080_chargectrl0 chargectrl0;

    ret = bq21080_read_reg(chip, BQ21080_REG_CHARGECTRL0, &chargectrl0.reg);
    if (ret != 0) {
        return ret;
    }

    if (millivolts > 4700) {
        chargectrl0.fields.vindpm = 0b11; // disable vindpm
    } else if (millivolts > 4500) {
        chargectrl0.fields.vindpm = 0b10; // 4.7V
    } else if (millivolts > 4200) {
        chargectrl0.fields.vindpm = 0b01; // 4.5V
    } else {
        chargectrl0.fields.vindpm = 0b00; // 4.2V
    }

    return bq21080_write_reg(chip, BQ21080_REG_CHARGECTRL0, chargectrl0.reg);

}

int bq21080_ship(bq21080* chip) {
    int ret;
    bq21080_ship_rst ship_rst;

    ret = bq21080_read_reg(chip, BQ21080_REG_SHIP_RST, &ship_rst.reg);
    if (ret != 0) {
        return ret;
    }

    ship_rst.fields.en_rst_ship = 0b10;
    return bq21080_write_reg(chip, BQ21080_REG_SHIP_RST, ship_rst.reg);
}

int bq21080_shutdown(bq21080* chip) {
    int ret;
    bq21080_ship_rst ship_rst;

    ret = bq21080_read_reg(chip, BQ21080_REG_SHIP_RST, &ship_rst.reg);
    if (ret != 0) {
        return ret;
    }

    ship_rst.fields.en_rst_ship = 0b01;
    return bq21080_write_reg(chip, BQ21080_REG_SHIP_RST, ship_rst.reg);
}

int bq21080_set_vsys_voltage(bq21080* chip, int millivolts) {
    int ret;
    bq21080_sys_reg sys_reg;

    ret = bq21080_read_reg(chip, BQ21080_REG_SYS_REG, &sys_reg.reg);
    if (ret != 0) {
        return ret;
    }

    if (millivolts > 4900) {
        sys_reg.fields.sys_reg_ctrl = 0b111; // pass through mode
    } else if (millivolts == 4900) {
        sys_reg.fields.sys_reg_ctrl = 0b110; // 4.9V
    } else if (millivolts >= 4800) {
        sys_reg.fields.sys_reg_ctrl = 0b101; // 4.8V
    } else if (millivolts >= 4700) {
        sys_reg.fields.sys_reg_ctrl = 0b100; // 4.7V
    } else if (millivolts >= 4600) {
        sys_reg.fields.sys_reg_ctrl = 0b011; // 4.6V
    } else if (millivolts >= 4500) {
        sys_reg.fields.sys_reg_ctrl = 0b010; // 4.5V
    } else if (millivolts >= 4400) {
        sys_reg.fields.sys_reg_ctrl = 0b001; // 4.4V
    } else {
        sys_reg.fields.sys_reg_ctrl = 0b000; // battery tracking mode
    }

    return bq21080_write_reg(chip, BQ21080_REG_SYS_REG, sys_reg.reg);
}

int bq21080_reset(bq21080* chip) {
    bq21080_ship_rst ship_rst = {0};

    ship_rst.fields.reg_rst = 0b1;
    return bq21080_write_reg(chip, BQ21080_REG_SHIP_RST, ship_rst.reg);
}

int bq21080_init(bq21080* chip, i2c_bus* bus, uint8_t addr) {
    int ret;
    bq21080_mask_id chip_id;

    chip->bus = bus;
    chip->addr = addr;

    ret = bq21080_reset(chip);
    if (ret != 0) {
        return ret;
    }

    ret = bq21080_read_id(chip, &chip_id.reg);
    if (ret != 0) {
        return ret;
    }

    if (chip_id.fields.device_id != BQ21080_DEVICE_ID) {
        return -ENOTSUP;
    }

    return 0;
}
