#pragma once

#include "driver/i2c.h"

#include "i2c.h"

typedef struct i2c_esp_bus {
    i2c_port_t port;
    unsigned int timeout;
    i2c_config_t* config;
} i2c_esp_bus;

int i2c_esp_init(i2c_bus* bus, i2c_config_t *config, i2c_port_t port, i2c_mode_t mode, unsigned int timeout);
int i2c_esp_deinit(i2c_bus* bus);
